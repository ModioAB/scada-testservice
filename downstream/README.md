# The downstream container

This is the container we take & wrap from another project, where we add the
required base libraries from a template, some mandatory files ( that should be
empty to ensure we can mount something useful over them ) and folder structures
to match.

We also add systemd service files here, as well as any timers and more.

# Build locally

If you have all the authentication to fetch upstream containers and want to use
the Makefile integration:

    UPSTREAM_CONTAINER=registry.gitlab.com/modioab/scada-testservice/upstream-rust:main  make build

Manually:

    `podman build --platform=linux/arm --build-arg UPSTREAM_CONTAINER=some.container/container:tag  .`

Although to make it pass through CI you would have to use more ARGS (see
Dockerfile for the details there)

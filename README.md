# Example code for modio containers
This repo contains two examples, one for [upstream](./upstream-rust/), which is a simple application (written in rust) packaged in a container, and a [downstream](./downstream/) that pulls a docker container and makes it ready to deploy to modio devices.

# The upstream project
This is a simple hello world service that will run forever and print some lines to logs and more.

The service has a systemd watchdog integration, to keep it running, but doesn't do much else.

There is no language requirement here, really, but rust has a non-trivial build system to generate statically compiled binaries that work, and is thus a good example.

See more details in [upstream rust readme](./upstream-rust/README.md)

# The downstream project
The downstream folder contains a simple Dockerfile to take the upstream project, add some proper paths and files that are needed, and wrap it up for integration test, packaging and deployment.

See more details in [downstream readme](./downstream/README.md)

# The CI pipelines
I've tried to split the CI files for each project into their own CI files, to make it visually clear what is needed for each project, see [.gitlab-ci.yml](./gitlab-ci.yml) and the reference files in each project.

# Short form requirements
1. Place statically linked binary in `/opt/bin/container-name`
2. Make sure `/opt/bin/container-name`  works on arm32 linux and is not an `x86_64` binary.
3. Make sure `/opt/bin/container-name --help` works and exits with success. ( it is used in CI )
4. If `/opt/bin/container-name` is dynamically linked, make the glibc version
   old enough to not accidentally surprise (debian10 is ok)
5. Make a systemd service to launch it, place in `/etc/systemd/system/container-name.service`
6. Make an empty file named `/etc/container-name.conf`
7. Create empty files /etc/os-release /etc/resolv.conf /etc/machine-id /etc/hosts
8. Publish container, trigger jobs to modio-contain with SERVICE=container-name and CONTAINER=registry.gitlab..../container-name/container:latest


# The on-device Containerized environment
These requirements are taken from the 
[Modio Contain](https://gitlab.com/ModioAB/modio-contain/) repo and are not automatically kept in sync.

## Installation target
The app being contained should be installed into /opt/ and only there, this includes all dependencies and assisting files.

## Service data and files
The app being contained will be launched under systemd, using Dynamic User, and various sandboxing and isolation features. This means that there will be a state directory available to store data on, while it also means that device access and more will be sandboxed. There will not be root access.

All services will have a config file available to them, whether they use it or not is up to the service.

## Removed paths
The following paths will be removed from contained services,, and if the containerised application has any dependencies on data in those, it should fail.

- /bin
- /sbin
- /lib
- /media
- /home
- /boot
- /root
- /usr

## Truncated files
The following files will be truncated, and other data will be mounted on top of them at runtime

- /etc/os-release
- /etc/resolv.conf
- /etc/machine-id
- /etc/hosts

## Mandatory paths inside the container
The path `/etc/container-name.conf` must exist and be empty. An outside-provided configuration file will be mounted into this path.

The path `/etc/systemd/system/container-name.service` must exist and contain a container-relative service file. Please make sure you point at the right path.

The path `/opt/bin/container-name` must exist and be executable, and must accept the `--help` command and return with exit-code 0 on it.

It can be a shell script wrapper (to set `LD_LIBRARY_PATH`, `PYTHONPATH`, or other setup) or a binary.

The CI system will call `/opt/bin/container-name --help`  during test, integration and build to ensure that the application works.

## Mandatory OCI container labels
- `se.modio.service` must be set to `container-name`  and must match the systemd `container-name.service`.
- `se.modio.ci.url` should point at the git repo so it  can be found, linked and tracked.
- `se.modio.ci.commit` should be the git commit that generated the container.
- `se.modio.ci.branch` should be the git branch.
- `se.modio.ci.date` should be the build date for the container.

# Other links
- A hello world style application with some base OS templating [Scada Hello](https://gitlab.com/ModioAB/scada-hello/)
- The [Modio Contain](https://gitlab.com/ModioAB/modio-contain/) repo itself  
- The [Modio MQTTBridge](https://gitlab.com/ModioAB/modio-mqttbridge/) repo is a service deployed like this.


# An example service in rust

This is an example contained service that does nothing much of use but shows
how to wrap it in a container and then wrap it in for shipping into a modio
contained project.

Running is simple `cargo run`  or `RUST_LOG=debug  cargo run`

The Dockerfile is just used as a wrapper for build & publish rather than build
as it is.

# Manually building the code for 32bit arm

Building the target (release) binary for arm32 in CI is done in a container, to
replicate the same steps locally I used:

    podman run -ti -v $(pwd):/build:rw,z  --workdir /build registry.gitlab.com/modioab/base-image/debian10/rust-cross:latest cargo build --target-dir  ./target --release -p testservice --target armv7-unknown-linux-gnueabihf

Step by step:

| argument       |   meaning                                                             |
|----------------|-----------------------------------------------------------------------|
|  -ti           |  tty interactive, see what happens                                    |
| -v ....        |  mount current dir to /build in container                             | 
| --workdir      |  make /build working dir in the container                             | 
| container name |  can be done with any container with arm32 target & build tools       | 
| cargo build    |  command inside container                                             |
| --target-dir ..|  ensure path is in /build/target so files end up in well known places | 
| --release      |  release build profile (optimizations, no debug)                      |
| -p testservice |  build only "testservice"                                             |
| --target ....  |  compile for armv7 linux                                              |



# Manually building container

First build the binary and then the container. CI uses the makefile integration
which handles login and such (`make build`)  but on the command line when
working, `podman build .` is enough to make a working container.

// Author: D.S. Ljungmark <spider@skuggor.se>, Modio FA AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use std::env;
use std::io::{self, ErrorKind};
use std::str::FromStr;
use std::time::Duration;
use tokio::net::UnixDatagram;

/// Pick up the socket path to notify and then send the message to it.
/// Details on the protocol are here <https://www.man7.org/linux/man-pages/man3/sd_notify.3.html>
pub async fn sd_notify(msg: &str) -> io::Result<()> {
    if let Some(socket_path) = env::var_os("NOTIFY_SOCKET") {
        let sock = UnixDatagram::unbound()?;
        let len = sock.send_to(msg.as_bytes(), socket_path).await?;
        if len == msg.len() {
            Ok(())
        } else {
            let err = io::Error::new(ErrorKind::WriteZero, "incomplete write");
            Err(err)
        }
    } else {
        Ok(())
    }
}

pub fn watchdog_time() -> Option<Duration> {
    if let Some(str_usecs) = env::var_os("WATCHDOG_USEC") {
        if let Some(str_usecs) = str_usecs.to_str() {
            if let Ok(val) = u64::from_str(str_usecs) {
                let res = Duration::from_micros(val);
                return Some(res);
            }
        }
    }
    None
}

/// Helper, send "READY=1"
pub async fn sd_ready() -> io::Result<()> {
    sd_notify("READY=1").await
}

/// Helper, send "WATCHDOG=1"
pub async fn ping_watchdog() -> io::Result<()> {
    sd_notify("WATCHDOG=1").await
}

pub async fn ticker() -> io::Result<()> {
    let watchdog_wait = match watchdog_time() {
        // Div with 1.2 to give us some leeway
        Some(duration) => {
            log::debug!("Original duration {:?}", duration);
            duration.div_f32(1.2)
        }
        None => Duration::from_secs(60),
    };
    log::info!("Waiting for {:?} between ticks", watchdog_wait);
    // interval ticks every duration, and takes into account the time between callss, thus it
    // sleeps for shorter time than Duration to ensure periodic evenness
    //
    let mut interval = tokio::time::interval(watchdog_wait);
    sd_ready().await?;
    // Tick interval once to prime it.
    interval.tick().await;
    loop {
        log::debug!("systemd watchdog sleeping");
        interval.tick().await;
        ping_watchdog().await?;
    }
}

mod sd_notify;
use argh::FromArgs;
use std::io;
use std::time::Duration;

const VERSION: &str = env!("CARGO_PKG_VERSION");
const CI_COMMIT_SHA: &str = match option_env!("CI_COMMIT_SHA") {
    Some(val) => val,
    None => "unknown",
};


#[derive(FromArgs, Debug)]
/// Command line args for scada-testservice
///
struct CmdLine {
    /// config file
    ///
    /// Path to the configuration file.
    /// Technically not used, but serves as a flag.
    #[argh(option, short = 'c')]
    config: Option<std::path::PathBuf>,
}


async fn does_some_work() -> io::Result<()> {
    let period = Duration::from_secs(13);
    let mut interval = tokio::time::interval(period);
    loop {
        interval.tick().await;
        log::info!("Doing some work here");
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    // Set up a logger with some defaults.
    env_logger::Builder::new()
        .format_timestamp_secs()
        .write_style(env_logger::WriteStyle::Auto)
        .parse_default_env()
        .init();
    let commandline: CmdLine = argh::from_env();

    // It's always a good idea to print the _version_ and commit when starting, as that allows logs
    // to check that the correct code is running.
    // More than once this has been a cause of confusion.
    println!("Version: {VERSION}, GIT_COMMIT_SHA={CI_COMMIT_SHA}");

    // Log the config file (so it is not unused and causes compile warnings)
    if let Some(file) = commandline.config {
        log::info!("Would open: {:?}", file);
    }

    // Spawn two futures, one to ping the watchdog and one to do some work
    let watchdog_future = sd_notify::ticker();
    let work_future = does_some_work();

    // Wait for any of them to be done, here it is an error if they do return Ok,
    // (they are supposed to run forever)
    // And otherwise it's another error, and we print that.
    let res = tokio::try_join!(work_future, watchdog_future);
    match res {
        Ok((_, _)) => unreachable!(),
        Err(e) => {
            log::error!("Error from task: {}", e);
            panic!("Error occured in some sub-routine.");
        }
    }
}
